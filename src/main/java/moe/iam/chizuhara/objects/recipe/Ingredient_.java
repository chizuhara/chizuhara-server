
package moe.iam.chizuhara.objects.recipe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "amount",
    "unit"
})
public class Ingredient_ implements Serializable
{

    @JsonProperty("name")
    private String name;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("unit")
    private String unit;
    private final static long serialVersionUID = -8306061905714418853L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Ingredient_() {
    }

    /**
     * 
     * @param amount
     * @param unit
     * @param name
     */
    public Ingredient_(String name, String amount, String unit) {
        super();
        this.name = name;
        this.amount = amount;
        this.unit = unit;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("unit")
    public String getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("amount", amount).append("unit", unit).toString();
    }

}
