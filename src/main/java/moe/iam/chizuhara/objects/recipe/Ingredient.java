
package moe.iam.chizuhara.objects.recipe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "part",
    "ingredients"
})
public class Ingredient implements Serializable
{

    @JsonProperty("part")
    private Integer part;
    @JsonProperty("ingredients")
    private List<Ingredient_> ingredients = null;
    private final static long serialVersionUID = 8094389080525499070L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Ingredient() {
    }

    /**
     * 
     * @param part
     * @param ingredients
     */
    public Ingredient(Integer part, List<Ingredient_> ingredients) {
        super();
        this.part = part;
        this.ingredients = ingredients;
    }

    @JsonProperty("part")
    public Integer getPart() {
        return part;
    }

    @JsonProperty("part")
    public void setPart(Integer part) {
        this.part = part;
    }

    @JsonProperty("ingredients")
    public List<Ingredient_> getIngredients() {
        return ingredients;
    }

    @JsonProperty("ingredients")
    public void setIngredients(List<Ingredient_> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("part", part).append("ingredients", ingredients).toString();
    }

}
