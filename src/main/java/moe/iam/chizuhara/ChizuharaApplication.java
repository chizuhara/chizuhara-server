package moe.iam.chizuhara;

import com.mongodb.MongoSocketException;
import moe.iam.chizuhara.db.MongoHandler;
import moe.iam.chizuhara.security.RateLimiter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChizuharaApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(ChizuharaApplication.class, args);
		} catch (MongoSocketException ignore) {}
		MongoHandler.init();
		RateLimiter.init();
	}

}
