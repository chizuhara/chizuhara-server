package moe.iam.chizuhara.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class InvalidCredentialsException extends RuntimeException {
    public InvalidCredentialsException(InvalidCredential invalidCredential) {
        super("Invalid credentials.");
    }

    public enum InvalidCredential {
        UNKNOWN,
        EMAIL,
        PASSWORD;
    }
}
