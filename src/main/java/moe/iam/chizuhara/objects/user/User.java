
package moe.iam.chizuhara.objects.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_id",
    "email",
    "password",
    "recipes",
    "token",
    "can_write",
    "can_read",
    "can_delete"
})
public class User implements Serializable
{

    @JsonProperty("_id")
    private String id;
    @JsonProperty("email")
    private String email;
    @JsonProperty("password")
    private String password;
    @JsonProperty(value = "recipes", required = false)
    private List<Recipe> recipes = null;
    @JsonProperty(value = "token", required = false)
    private List<Token> token = null;
    @JsonProperty(value = "can_write", required = false)
    private Boolean canWrite;
    @JsonProperty(value = "can_read", required = false)
    private Boolean canRead;
    @JsonProperty(value = "can_delete", required = false)
    private Boolean canDelete;
    private final static long serialVersionUID = 8512982275712654248L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public User() {
    }

    /**
     * 
     * @param recipes
     * @param password
     * @param canRead
     * @param canWrite
     * @param canDelete
     * @param id
     * @param email
     * @param token
     */
    public User(String id, String email, String password, List<Recipe> recipes, List<Token> token, Boolean canWrite, Boolean canRead, Boolean canDelete) {
        super();
        this.id = id;
        this.email = email;
        this.password = password;
        this.recipes = recipes;
        this.token = token;
        this.canWrite = canWrite;
        this.canRead = canRead;
        this.canDelete = canDelete;
    }

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("_id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("recipes")
    public List<Recipe> getRecipes() {
        return recipes;
    }

    @JsonProperty("recipes")
    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    @JsonProperty("token")
    public List<Token> getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(List<Token> token) {
        this.token = token;
    }

    @JsonProperty("can_write")
    public Boolean getCanWrite() {
        return canWrite;
    }

    @JsonProperty("can_write")
    public void setCanWrite(Boolean canWrite) {
        this.canWrite = canWrite;
    }

    @JsonProperty("can_read")
    public Boolean getCanRead() {
        return canRead;
    }

    @JsonProperty("can_read")
    public void setCanRead(Boolean canRead) {
        this.canRead = canRead;
    }

    @JsonProperty("can_delete")
    public Boolean getCanDelete() {
        return canDelete;
    }

    @JsonProperty("can_delete")
    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("email", email).append("password", password).append("recipes", recipes).append("token", token).append("canWrite", canWrite).append("canRead", canRead).append("canDelete", canDelete).toString();
    }

}
