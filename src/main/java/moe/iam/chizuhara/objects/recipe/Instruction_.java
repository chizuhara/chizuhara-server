
package moe.iam.chizuhara.objects.recipe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "step",
    "title",
    "instruction"
})
public class Instruction_ implements Serializable
{

    @JsonProperty("step")
    private Integer step;
    @JsonProperty("title")
    private String title;
    @JsonProperty("instruction")
    private String instruction;
    private final static long serialVersionUID = 563868071266239296L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Instruction_() {
    }

    /**
     * 
     * @param instruction
     * @param step
     * @param title
     */
    public Instruction_(Integer step, String title, String instruction) {
        super();
        this.step = step;
        this.title = title;
        this.instruction = instruction;
    }

    @JsonProperty("step")
    public Integer getStep() {
        return step;
    }

    @JsonProperty("step")
    public void setStep(Integer step) {
        this.step = step;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("instruction")
    public String getInstruction() {
        return instruction;
    }

    @JsonProperty("instruction")
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("step", step).append("title", title).append("instruction", instruction).toString();
    }

}
