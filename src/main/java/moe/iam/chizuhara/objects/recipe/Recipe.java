
package moe.iam.chizuhara.objects.recipe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.mongojack.ObjectId;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_id",
    "title",
    "yields",
    "prep_time",
    "total_time",
    "image",
    "parts",
    "ingredients",
    "instructions"
})
public class Recipe implements Serializable {

    @ObjectId
    @JsonProperty("_id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("yields")
    private Integer yields;
    @JsonProperty("prep_time")
    private Integer prepTime;
    @JsonProperty("total_time")
    private Integer totalTime;
    @JsonProperty("image")
    private String image;
    @JsonProperty("parts")
    private List<Part> parts = null;
    @JsonProperty("ingredients")
    private List<Ingredient> ingredients = null;
    @JsonProperty("instructions")
    private List<Instruction> instructions = null;
    private Integer prepTimeHours;
    private Integer prepTimeMinutes;
    private Integer totalTimeHours;
    private Integer totalTimeMinutes;
    private final static long serialVersionUID = -6593810566744073766L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Recipe() {
    }

    /**
     * 
     * @param image
     * @param instructions
     * @param totalTime
     * @param yields
     * @param parts
     * @param ingredients
     * @param id
     * @param title
     * @param prepTime
     */
    public Recipe(String id, String title, Integer yields, Integer prepTime, Integer totalTime, String image, List<Part> parts, List<Ingredient> ingredients, List<Instruction> instructions) {
        super();
        this.id = id;
        this.title = title;
        this.yields = yields;
        this.prepTime = prepTime;
        this.totalTime = totalTime;
        this.image = image;
        this.parts = parts;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("_id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("yields")
    public Integer getYields() {
        return yields;
    }

    @JsonProperty("yields")
    public void setYields(Integer yields) {
        this.yields = yields;
    }

    @JsonProperty("prep_time")
    public Integer getPrepTime() {
        return prepTime;
    }

    @JsonProperty("prep_time")
    public void setPrepTime(Integer prepTime) {
        this.prepTime = prepTime;
    }

    @JsonProperty("total_time")
    public Integer getTotalTime() {
        return totalTime;
    }

    @JsonProperty("total_time")
    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("parts")
    public List<Part> getParts() {
        return parts;
    }

    @JsonProperty("parts")
    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    @JsonProperty("ingredients")
    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    @JsonProperty("ingredients")
    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @JsonProperty("instructions")
    public List<Instruction> getInstructions() {
        return instructions;
    }

    @JsonProperty("instructions")
    public void setInstructions(List<Instruction> instructions) {
        this.instructions = instructions;
    }

    public Integer getPrepTimeHours() {
        return prepTimeHours;
    }

    public void setPrepTimeHours(Integer prepTimeHours) {
        this.prepTimeHours = prepTimeHours;
    }

    public Integer getPrepTimeMinutes() {
        return prepTimeMinutes;
    }

    public void setPrepTimeMinutes(Integer prepTimeMinutes) {
        this.prepTimeMinutes = prepTimeMinutes;
    }

    public Integer getTotalTimeHours() {
        return totalTimeHours;
    }

    public void setTotalTimeHours(Integer totalTimeHours) {
        this.totalTimeHours = totalTimeHours;
    }

    public Integer getTotalTimeMinutes() {
        return totalTimeMinutes;
    }

    public void setTotalTimeMinutes(Integer totalTimeMinutes) {
        this.totalTimeMinutes = totalTimeMinutes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("title", title).append("yields", yields).append("prepTime", prepTime).append("totalTime", totalTime).append("image", image).append("parts", parts).append("ingredients", ingredients).append("instructions", instructions).toString();
    }

}
