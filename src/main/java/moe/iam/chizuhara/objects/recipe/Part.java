
package moe.iam.chizuhara.objects.recipe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title"
})
public class Part implements Serializable
{

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    private final static long serialVersionUID = -1965711258581000637L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Part() {
    }

    /**
     * 
     * @param id
     * @param title
     */
    public Part(Integer id, String title) {
        super();
        this.id = id;
        this.title = title;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("title", title).toString();
    }

}
