package moe.iam.chizuhara.endpoints.open;

import at.favre.lib.crypto.bcrypt.BCrypt;
import moe.iam.chizuhara.db.UserHandler;
import moe.iam.chizuhara.exceptions.InvalidCredentialsException;
import moe.iam.chizuhara.objects.user.Token;
import moe.iam.chizuhara.objects.user.User;
import moe.iam.chizuhara.security.RateLimiter;
import moe.iam.chizuhara.utils.Methods;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CreateAccount {

    @CrossOrigin
    @PostMapping("/createAccount")
    public ResponseEntity<String> createAccount(@RequestBody String newUser, @RequestHeader(value = "session-token", required = false) String session_token) {
        if (!RateLimiter.canRequest(session_token, 5)) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }

        /*
        Response variables
         */
        boolean passwordTooWeak = false;
        boolean invalidEmail = false;
        boolean emailAlreadyInUse = false;

        /*
        Parse user credentials
         */
        JSONParser parser = new JSONParser();
        JSONObject userJson = null;
        try {
            userJson = (JSONObject) parser.parse(newUser);
        } catch (ParseException ignore) {}
        if (userJson == null || !userJson.containsKey("email") || !userJson.containsKey("password")) throw new InvalidCredentialsException(InvalidCredentialsException.InvalidCredential.UNKNOWN);

        /*
        Check if email is already used
         */
        User user = UserHandler.getUserFromEmail(String.valueOf(userJson.get("email")));
        if (user != null) emailAlreadyInUse = true;

        /*
        Server side password security check
         */
        String psw = String.valueOf(userJson.get("password"));
        if (psw.length() < 8) passwordTooWeak = true;

        /*
        Server side email check
         */
        String email = String.valueOf(userJson.get("email"));
        if (!Methods.isValidEmail(email)) invalidEmail = true;

        /*
        Check if everything is valid
         */
        if (!(passwordTooWeak && invalidEmail && emailAlreadyInUse)) {
            JSONObject response = new JSONObject();
            response.put("weak_password", passwordTooWeak);
            response.put("invalid_email", invalidEmail);
            response.put("email_already_in_use", emailAlreadyInUse);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(response.toJSONString());
        }

        /*
        Create new user object
         */
        user = new User();
        user.setEmail(email);
        String hashedPassword = BCrypt.withDefaults().hashToString(12, psw.toCharArray());
        user.setPassword(hashedPassword);
        user.setToken(new ArrayList<>());
        user.setRecipes(new ArrayList<>());
        user.setCanRead(true);
        user.setCanWrite(false);
        user.setCanDelete(false);

        /*
        Save user to Mongo DB
         */
        ObjectId id = UserHandler.createUser(user);
        if (id == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not create user");

        /*
        Create Token for user
         */
        Token token = Methods.generateJWT(id.toString());
        UserHandler.addTokenToUser(id.toString(), token);
        return ResponseEntity.ok(Methods.generateJsonResponse(token, user).toJSONString());
    }
}
