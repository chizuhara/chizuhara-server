package moe.iam.chizuhara.db;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.result.UpdateResult;
import moe.iam.chizuhara.exceptions.UserNotFoundException;
import moe.iam.chizuhara.objects.user.Token;
import moe.iam.chizuhara.objects.user.User;
import moe.iam.chizuhara.utils.Methods;
import org.bson.BsonObjectId;
import org.bson.BsonValue;
import org.bson.UuidRepresentation;
import org.bson.types.ObjectId;
import org.mongojack.JacksonMongoCollection;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

public class UserHandler {


    /**
     * Get the users collection
     * @return returns the user collection as a Jackson collection
     */
    public static JacksonMongoCollection<User> getUserCollection() {
        JacksonMongoCollection<User> collection = JacksonMongoCollection.builder().build(MongoHandler.getDatabase(),"users", User.class, UuidRepresentation.STANDARD);
        return collection;
    }

    /**
     * This will remove all the old token from a given list
     * @param token A list of token
     * @return The list of all still valid token
     */
    public static List<Token> removeOldToken(List<Token> token) {
        token.removeIf(foo -> Methods.getTokenDate(foo.getExpiration()).after(new Date(System.currentTimeMillis())));
        return token;
    }

    /**
     * Sets or overwrites which tokens are valid for a given user
     * @param userId The id of the user
     * @param token A list of all valid token
     * @return returns true if successful and false if unsuccessful
     */
    public static boolean setToken(String userId, List<Token> token) {
        JacksonMongoCollection<User> coll = getUserCollection();
        ObjectId id = new ObjectId(userId);
        User user = coll.findOneById(id);
        if (user == null) return false;
        user.setToken(token);
        user.setId(null);
        coll.replaceOneById(id, user);
        return true;
    }

    /**
     * Get a user from a session token
     * @param token the session token belonging to a user
     * @return the user that the given token belongs to or null if no user was found
     */
    public static User getUserFromToken(String token) {
        JacksonMongoCollection<User> coll = getUserCollection();
        coll.createIndex(Indexes.text("token.token"));
        User user = coll.findOne(Filters.text(token));
        if (user == null) return null;
        List<Token> tokenList = user.getToken();
        Token tokenObject = null;
        try {
            tokenObject = tokenList.stream().filter(tkn -> !isTokenExpired(tkn)).findFirst().get();
        } catch (NoSuchElementException ignored) {}
        if (tokenObject == null) return null; //maybe remove old tokens here?
        return user;
    }

    /**
     * Get a user using their email
     * @param email The email associated with the account
     * @return returns either a user or null if no user was found using that email
     */
    public static User getUserFromEmail(String email) {
        JacksonMongoCollection<User> coll = getUserCollection();
        return coll.find(Filters.eq("email", email)).first();
    }

    /**
     * Get a user using their id
     * @param userId The id of the user
     * @return returns the user with the given id or null if no user was found with that id
     */
    public static User getUserFromId(String userId) {
        return getUserFromId(new ObjectId(userId));
    }

    public static User getUserFromId(ObjectId userId) {
        JacksonMongoCollection<User> coll = getUserCollection();
        return coll.findOneById(userId);
    }

    /**
     * Checks if a token is expired, THIS WONT CHECK IF THE TOKEN IS VALID!
     * @param token the token as a {@link Token} Object
     * @return returns true if token is expired
     */
    public static boolean isTokenExpired(Token token) {
        return Methods.getTokenDate(token.getExpiration()).before(new Date(System.currentTimeMillis()));
    }

    /**
     * invalidates the given token
     * @param sessionToken the token that the user was using before logging out
     * @return returns true if user was logged out successfully
     */
    public static boolean logout(String sessionToken) {
        User user = getUserFromToken(sessionToken);
        if (user == null) return false;
        List<Token> tokens = user.getToken();
        tokens.removeIf(token -> token.getToken().equals(sessionToken));
        return setToken(user.getId(), tokens);
    }

    /**
     * Adds a token to user
     * @param userId the id of the user
     * @param token the token that you want to add to the user
     */
    public static void addTokenToUser(String userId, Token token) {
        User user = getUserFromId(userId);
        if (user == null) throw new UserNotFoundException();
        List<Token> validToken = user.getToken();
        validToken.removeIf(UserHandler::isTokenExpired);
        validToken.add(token);
        user.setToken(validToken);
        updateUser(user);
    }

    /**
     * Update a user in the mongo database
     * @param user the User object you want to update
     */
    public static void updateUser(User user) {
        JacksonMongoCollection<User> coll = getUserCollection();
        if (user.getId() == null) throw new UserNotFoundException();
        ObjectId userId = new ObjectId(user.getId());
        user.setId(null);
        UpdateResult result = coll.replaceOneById(userId, user);
        if (!result.wasAcknowledged()) throw new UserNotFoundException();
    }

    /**
     * Create a new user
     * @param user The user you want to create
     * @return Returns the id of the new user as {@link ObjectId}
     */
    public static ObjectId createUser(User user) {
        JacksonMongoCollection<User> coll = getUserCollection();
        BsonValue objId = coll.insertOne(user).getInsertedId();
        if (objId == null) return null;
        BsonObjectId bsonObjectId = objId.asObjectId();
        if (bsonObjectId == null) return null;
        return bsonObjectId.getValue();
    }
}
