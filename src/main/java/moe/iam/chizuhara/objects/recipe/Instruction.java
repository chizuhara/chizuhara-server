
package moe.iam.chizuhara.objects.recipe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "part",
    "instructions"
})
public class Instruction implements Serializable
{

    @JsonProperty("part")
    private Integer part;
    @JsonProperty("instructions")
    private List<Instruction_> instructions = null;
    private final static long serialVersionUID = 7283217651087209374L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Instruction() {
    }

    /**
     * 
     * @param instructions
     * @param part
     */
    public Instruction(Integer part, List<Instruction_> instructions) {
        super();
        this.part = part;
        this.instructions = instructions;
    }

    @JsonProperty("part")
    public Integer getPart() {
        return part;
    }

    @JsonProperty("part")
    public void setPart(Integer part) {
        this.part = part;
    }

    @JsonProperty("instructions")
    public List<Instruction_> getInstructions() {
        return instructions;
    }

    @JsonProperty("instructions")
    public void setInstructions(List<Instruction_> instructions) {
        this.instructions = instructions;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("part", part).append("instructions", instructions).toString();
    }

}
