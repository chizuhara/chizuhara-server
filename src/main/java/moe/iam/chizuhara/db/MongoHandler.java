package moe.iam.chizuhara.db;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import moe.iam.chizuhara.objects.recipe.Recipe;
import moe.iam.chizuhara.objects.user.User;
import org.bson.UuidRepresentation;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.mongojack.JacksonMongoCollection;

import java.util.ArrayList;

public class MongoHandler {

    private final static String DATABASE_TYPE = System.getenv("DATABASE_TYPE");
    private final static String USERNAME = System.getenv("MONGO_USERNAME");
    private final static String PASSWORD = System.getenv("MONGO_PASSWORD");
    private final static String CLUSTER_ADDRESS = System.getenv("MONGO_ADDRESS");
    private static String USE_SSL = System.getenv("USE_SSL");
    private static ConnectionString connString = null;
    private static final String DATABASE = "chizuhara";

    public static void init() {
        if (USE_SSL == null) USE_SSL = "true";
        if (DATABASE_TYPE.equalsIgnoreCase("CLOUD"))
            connString = new ConnectionString(
                    "mongodb+srv://" + USERNAME + ":" + PASSWORD + "@" + CLUSTER_ADDRESS + "/test?w=majority"
            );
        else {
            connString = new ConnectionString("mongodb://" + USERNAME + ":" + PASSWORD + "@" + CLUSTER_ADDRESS + "/?ssl=" + USE_SSL);
        }
    }

    /**
     * Get the MongoDB
     * @return returns the mongo database
     */
    public static MongoDatabase getDatabase() {
        MongoClient mongo = MongoClients.create(connString);
        MongoDatabase database = mongo.getDatabase(DATABASE);
        return database;
    }

    @Deprecated
    public static Recipe getRecipeFromId(String id) {
        MongoClient mongo = MongoClients.create(connString);
        MongoDatabase database = mongo.getDatabase(DATABASE);
        JacksonMongoCollection<Recipe> coll = JacksonMongoCollection.builder().build(database,"recipes", Recipe.class, UuidRepresentation.STANDARD);
        Recipe recipe = coll.find(Filters.eq("_id", id)).first();
        mongo.close();
        return recipe;
    }

    @Deprecated
    public static ArrayList<Recipe> getAllRecipes() {
        ArrayList<Recipe> recipeList = new ArrayList<>();
        MongoClient mongo = MongoClients.create(connString);
        MongoDatabase database = mongo.getDatabase(DATABASE);
        JacksonMongoCollection<Recipe> coll = JacksonMongoCollection.builder().build(database,"recipes", Recipe.class, UuidRepresentation.STANDARD);
        for (Recipe recipe : coll.find()) {
            recipeList.add(recipe);
        }
        mongo.close();
        return recipeList;
    }

    @Deprecated
    public static void createRecipe(Recipe recipe) {
        MongoClient mongo = MongoClients.create(connString);
        MongoDatabase database = mongo.getDatabase(DATABASE);
        JacksonMongoCollection<Recipe> coll = JacksonMongoCollection.builder().build(database,"recipes", Recipe.class, UuidRepresentation.STANDARD);
        coll.insertOne(recipe);
        mongo.close();
    }

    @Deprecated
    public static User getUserFromJsonToken(String jsonString) {
        JSONParser parser = new JSONParser();
        JSONObject json = null;
        try {
            json = (JSONObject) parser.parse(jsonString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (json == null || !json.containsKey("token")) return null;
        MongoClient mongo = MongoClients.create(connString);
        MongoDatabase database = mongo.getDatabase(DATABASE);
        JacksonMongoCollection<User> coll = JacksonMongoCollection.builder().build(database,"users", User.class, UuidRepresentation.STANDARD);
        User user = coll.findOne(Filters.eq("token.token", json.get("token")));
        return user;
    }
}
