package moe.iam.chizuhara.endpoints.open;

import at.favre.lib.crypto.bcrypt.BCrypt;
import moe.iam.chizuhara.db.UserHandler;
import moe.iam.chizuhara.exceptions.InvalidCredentialsException;
import moe.iam.chizuhara.exceptions.UserNotFoundException;
import moe.iam.chizuhara.objects.user.Token;
import moe.iam.chizuhara.objects.user.User;
import moe.iam.chizuhara.security.RateLimiter;
import moe.iam.chizuhara.utils.Methods;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Authenticate {


    @CrossOrigin
    @PostMapping("/authenticate")
    public ResponseEntity<String> authenticate(@RequestBody String userCredentials, @RequestHeader(value = "session-token", required = false) String session_token) {
        if (!RateLimiter.canRequest(session_token, 3)) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }

        /*
        Parse user credentials
         */
        JSONParser parser = new JSONParser();
        JSONObject userJson = null;
        try {
            userJson = (JSONObject) parser.parse(userCredentials);
        } catch (ParseException ignore) {}
        if (userJson == null || !userJson.containsKey("email") || !userJson.containsKey("password")) throw new InvalidCredentialsException(InvalidCredentialsException.InvalidCredential.UNKNOWN);
        User user = UserHandler.getUserFromEmail(String.valueOf(userJson.get("email")));
        if (user == null) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

        /*
        Check if password matches hashed password from the db
         */
        BCrypt.Result result = BCrypt.verifyer().verify(String.valueOf(userJson.get("password")).toCharArray(), user.getPassword());
        if (!result.verified) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

        /*
        Generate a new Token and save it in the db
         */
        Token token = Methods.generateJWT(user.getId());
        try {
            UserHandler.addTokenToUser(user.getId(), token);
        } catch (UserNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(Methods.generateJsonResponse(token, user).toJSONString());
    }
}
