
package moe.iam.chizuhara.objects.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "rating",
    "marked"
})
public class Recipe implements Serializable
{

    @JsonProperty("id")
    private String id;
    @JsonProperty("rating")
    private Integer rating;
    @JsonProperty("marked")
    private Boolean marked;
    private final static long serialVersionUID = 9087246209271561873L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Recipe() {
    }

    /**
     * 
     * @param marked
     * @param rating
     * @param id
     */
    public Recipe(String id, Integer rating, Boolean marked) {
        super();
        this.id = id;
        this.rating = rating;
        this.marked = marked;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("rating")
    public Integer getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @JsonProperty("marked")
    public Boolean getMarked() {
        return marked;
    }

    @JsonProperty("marked")
    public void setMarked(Boolean marked) {
        this.marked = marked;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("rating", rating).append("marked", marked).toString();
    }

}
