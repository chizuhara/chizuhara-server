package moe.iam.chizuhara.utils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import moe.iam.chizuhara.objects.user.Token;
import moe.iam.chizuhara.objects.user.User;
import org.json.simple.JSONObject;

import java.security.Key;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class Methods {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Create a session token using the user id
     * @param userId The id of the user
     * @return Returns the token as a {@link Token} object
     */
    public static Token generateJWT(String userId) {
        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

        Calendar expirationCalendar = Calendar.getInstance();
        expirationCalendar.add(Calendar.MONTH, 6);
        Date expiration = expirationCalendar.getTime();

        String jws = Jwts.builder().setExpiration(expiration)
                .setSubject(userId)
                .signWith(key)
                .compact();

        Token token = new Token();
        token.setToken(jws);
        token.setExpiration(dateFormat.format(expiration));
        return token;
    }

    public static Date getTokenDate(String stringDate) {
        Date date = null;
        try {
            date = dateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static JSONObject generateJsonResponse(Token token, User user) {
        JSONObject json = new JSONObject();
        json.put("token", token.getToken());
        json.put("expiration", token.getExpiration());
        JSONObject permissions = new JSONObject();
        permissions.put("canRead", user.getCanRead());
        permissions.put("canWrite", user.getCanWrite());
        permissions.put("canDelete", user.getCanDelete());
        json.put("permissions", permissions);
        return json;
    }

    public static boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailRegex);
        if (email == null) return false;
        return pattern.matcher(email).matches();
    }
}
