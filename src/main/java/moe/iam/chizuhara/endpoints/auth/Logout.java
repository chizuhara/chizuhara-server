package moe.iam.chizuhara.endpoints.auth;

import moe.iam.chizuhara.db.UserHandler;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Logout {

    @CrossOrigin
    @GetMapping("/logout")
    public Object getAllRecipesWithRating(@RequestHeader("session-token") String session_token) {
        if (UserHandler.logout(session_token)) return "Successfully logged out!";
        return "Could not log user out!";
    }
}
