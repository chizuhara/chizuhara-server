package moe.iam.chizuhara.db;

import com.mongodb.client.result.InsertOneResult;
import moe.iam.chizuhara.objects.recipe.Recipe;
import org.bson.UuidRepresentation;
import org.bson.types.ObjectId;
import org.mongojack.JacksonMongoCollection;

import java.util.ArrayList;

public class RecipeHandler {

    /**
     * Get the recipes collection
     * @return returns the user collection as a Jackson collection
     */
    public static JacksonMongoCollection<Recipe> getRecipesCollection() {
        JacksonMongoCollection<Recipe> collection = JacksonMongoCollection.builder().build(MongoHandler.getDatabase(),"recipes", Recipe.class, UuidRepresentation.STANDARD);
        return collection;
    }

    /**
     * Get a recipe using the recipe id
     * @param id The id of the recipe
     * @return returns the recipe with the given id or null if no recipe was found
     */
    public static Recipe getRecipeFromId(String id) {
        return getRecipeFromId(new ObjectId(id));
    }

    public static Recipe getRecipeFromId(ObjectId id) {
        JacksonMongoCollection<Recipe> coll = getRecipesCollection();
        return coll.findOneById(id);
    }

    /**
     * Get all recipes
     * @return Returns a list of all recipes
     */
    public static ArrayList<Recipe> getAllRecipes() {
        ArrayList<Recipe> recipeList = new ArrayList<>();
        JacksonMongoCollection<Recipe> coll = getRecipesCollection();
        for (Recipe recipe : coll.find()) {
            recipeList.add(recipe);
        }
        return recipeList;
    }

    /**
     * Get a specific amount of recipes
     * @param recipes how many recipes you want to get
     * @return Returns the recipes as a list
     */
    public static ArrayList<Recipe> getRecipes(int recipes) {
        ArrayList<Recipe> recipeList = new ArrayList<>();
        JacksonMongoCollection<Recipe> coll = getRecipesCollection();
        for (Recipe recipe : coll.find().limit(recipes)) {
            recipeList.add(recipe);
        }
        return recipeList;
    }

    /**
     * Saves a new recipe to the database
     * @param recipe The recipe you want to save
     * @return returns the id of the recipe
     */
    public static ObjectId createRecipe(Recipe recipe) {
        JacksonMongoCollection<Recipe> coll = getRecipesCollection();
        InsertOneResult result = coll.insertOne(recipe);
        if (result.getInsertedId() == null) return null;
        return result.getInsertedId().asObjectId().getValue();
    }
}
