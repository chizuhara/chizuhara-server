package moe.iam.chizuhara.endpoints.auth;


import moe.iam.chizuhara.db.MongoHandler;
import moe.iam.chizuhara.objects.user.User;
import moe.iam.chizuhara.security.RateLimiter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecipeRatings {

    @CrossOrigin
    @PostMapping("/recipesWithRatings")
    public Object getAllRecipesWithRating(@RequestBody String json, @RequestHeader("session-token") String session_token) {
        if (!RateLimiter.canRequest(session_token, 2)) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS);
        }
        User user = MongoHandler.getUserFromJsonToken(json);
        if (user != null && user.getRecipes() != null)
            return user.getRecipes();
        return null;
    }
}
