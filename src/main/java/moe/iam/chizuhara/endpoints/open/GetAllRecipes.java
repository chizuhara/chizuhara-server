package moe.iam.chizuhara.endpoints.open;

import moe.iam.chizuhara.db.RecipeHandler;
import moe.iam.chizuhara.objects.recipe.Recipe;
import moe.iam.chizuhara.security.RateLimiter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class GetAllRecipes {

    @CrossOrigin
    @GetMapping("/recipes")
    public ResponseEntity<ArrayList<Recipe>> greeting(@RequestHeader(value = "session-token", required = false) String session_token) {
        if (!RateLimiter.canRequest(session_token, 3)) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }
        ArrayList<Recipe> recipes = RecipeHandler.getRecipes(10);
        if (recipes.isEmpty()) ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find any recipes!");
        return ResponseEntity.ok(recipes);
    }
}
