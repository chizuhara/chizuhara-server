package moe.iam.chizuhara.endpoints.open;

import moe.iam.chizuhara.security.RateLimiter;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CalculateTime {

    @CrossOrigin
    @GetMapping("/calculateTime")
    public Object endPoint(@RequestParam(value = "time", defaultValue = "") String time, @RequestHeader(value = "session-token", required = false) String session_token) {
        if (!RateLimiter.canRequest(session_token, 1)) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS);
        }
        Integer timeInt = Integer.parseInt(time);
        if (timeInt == null || timeInt < 0) {
            return "Invalid time!";
        }
        JSONObject obj = new JSONObject();
        obj.put("hours", Math.floor(timeInt / 60));
        obj.put("minutes", timeInt % 60);
        return obj;
    }
}
