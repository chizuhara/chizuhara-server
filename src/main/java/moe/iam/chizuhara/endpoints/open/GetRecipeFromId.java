package moe.iam.chizuhara.endpoints.open;

import moe.iam.chizuhara.db.MongoHandler;
import moe.iam.chizuhara.objects.recipe.Recipe;
import moe.iam.chizuhara.security.RateLimiter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GetRecipeFromId {

    @CrossOrigin
    @GetMapping("/recipe")
    public ResponseEntity<Recipe> getRecipeFromId(@RequestParam(value = "id", defaultValue = "") String id, @RequestHeader(value = "session-token", required = false) String session_token) {
        if (!RateLimiter.canRequest(session_token, 1)) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }
        Recipe result = MongoHandler.getRecipeFromId(id);
        if (result == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        if (result.getPrepTimeHours() == null || result.getPrepTimeMinutes() == null ||
                result.getTotalTimeHours() == null || result.getTotalTimeMinutes() == null) {
            result.setTotalTimeHours((int) Math.floor(result.getTotalTime() / 60.0));
            result.setTotalTimeMinutes(result.getTotalTime() % 60);

            result.setPrepTimeHours((int) Math.floor(result.getPrepTime() / 60.0));
            result.setPrepTimeMinutes(result.getPrepTime() % 60);
        }
        return ResponseEntity.ok(result);
    }
}
