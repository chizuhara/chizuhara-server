package moe.iam.chizuhara.endpoints.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import moe.iam.chizuhara.db.RecipeHandler;
import moe.iam.chizuhara.db.UserHandler;
import moe.iam.chizuhara.objects.recipe.Recipe;
import moe.iam.chizuhara.objects.user.User;
import moe.iam.chizuhara.security.RateLimiter;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateRecipe {

    @PostMapping(value = "/createRecipe", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> createRecipe(@RequestBody String recipe, @RequestHeader("session-token") String session_token) {
        if (!RateLimiter.canRequest(session_token, 2)) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }
        ObjectMapper mapper = new ObjectMapper();
        Recipe parsedRecipe = null;
        User user = UserHandler.getUserFromToken(session_token);
        if (user == null || !user.getCanWrite()) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        try {
            parsedRecipe = mapper.readValue(recipe, Recipe.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not parse recipe");
        }
        if (parsedRecipe == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not parse recipe");
        }
        ObjectId recipeId = RecipeHandler.createRecipe(parsedRecipe);
        if (recipeId == null) return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not save recipe.");
        return ResponseEntity.ok(recipeId.toString());
    }
}
