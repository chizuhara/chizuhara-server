
package moe.iam.chizuhara.objects.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "expiration",
    "token"
})
public class Token implements Serializable {

    @JsonProperty("expiration")
    private String expiration;
    @JsonProperty("token")
    private String token;
    private final static long serialVersionUID = 5870061537916645523L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Token() {
    }

    /**
     * 
     * @param expiration
     * @param token
     */
    public Token(String expiration, String token) {
        super();
        this.expiration = expiration;
        this.token = token;
    }

    @JsonProperty("expiration")
    public String getExpiration() {
        return expiration;
    }

    @JsonProperty("expiration")
    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("expiration", expiration).append("token", token).toString();
    }

}
