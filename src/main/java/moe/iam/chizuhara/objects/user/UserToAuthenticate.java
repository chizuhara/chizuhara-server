
package moe.iam.chizuhara.objects.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "email",
    "password"
})
@Deprecated
public class UserToAuthenticate implements Serializable
{

    @JsonProperty("email")
    private String email;
    @JsonProperty("password")
    private String password;
    private final static long serialVersionUID = -4764314870224208459L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserToAuthenticate() {
    }

    /**
     * 
     * @param password
     * @param email
     */
    public UserToAuthenticate(String email, String password) {
        super();
        this.email = email;
        this.password = password;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("email", email).append("password", password).toString();
    }

}
