package moe.iam.chizuhara.security;

import io.github.bucket4j.*;
import moe.iam.chizuhara.db.UserHandler;
import moe.iam.chizuhara.objects.user.User;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RateLimiter {

    private static final Map<String, Bucket> cache = new ConcurrentHashMap<>();
    private static Bucket guestBucket = null;

    /**
     * Initializes the guest bucket
     */
    public static void init() {
        Refill refill = Refill.intervally(600, Duration.ofHours(1)); //how many new requests guests get each minute
        Bandwidth limit = Bandwidth.classic(60, refill); //how many requests should a guest start with
        guestBucket =  Bucket4j.builder()
                .addLimit(limit)
                .build();
    }

    /**
     * creates a new rate limit bucket
     * User starts with 10 requests, gets 10 new requests each minute
     * @return returns the rate limit bucket
     */
    private static Bucket getNewBucket() {
        Refill refill = Refill.intervally(30, Duration.ofMinutes(1)); //how many new requests a user gets each minute
        Bandwidth limit = Bandwidth.classic(30, refill); //how many requests should the user start with
        return Bucket4j.builder()
                .addLimit(limit)
                .build();
    }

    /**
     * Checks if the user has reached their rate limit
     * @param userId The id of the user
     * @return returns true if the user can use the API, returns false if they hit their rate limit
     */
    public static boolean canUserRequest(String userId) {
        if (!cache.containsKey(userId)) {
            cache.put(userId, getNewBucket());
            return true;
        }
        Bucket bucket = cache.get(userId);
        ConsumptionProbe probe = bucket.tryConsumeAndReturnRemaining(1);
        return probe.isConsumed();
    }

    /**
     * Checks whether a guest can afford to do an action
     * @param cost the cost of the action
     * @return returns true if the guest can afford the action
     */
    public static boolean canGuestRequest(int cost) {
        if (guestBucket == null) {
            init();
            System.out.println("GUEST BUCKET WAS NOT INITIALIZED!");
            return false;
        }
        ConsumptionProbe probe = guestBucket.tryConsumeAndReturnRemaining(cost);
        return probe.isConsumed();
    }

    public static boolean canRequest(String sessionToken, int cost) {
        if (sessionToken == null || sessionToken.length() == 0) return canGuestRequest(cost);
        User user = UserHandler.getUserFromToken(sessionToken);
        if (user == null) return canGuestRequest(cost);
        return canUserRequest(user.getId());
    }
}
