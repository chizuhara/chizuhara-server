#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM openjdk:11
COPY --from=build /home/app/target/chizuhara*.jar /usr/local/lib/chizuhara.jar
EXPOSE 8080
WORKDIR /usr/local/lib/
VOLUME /usr/local/lib/
ENTRYPOINT ["java","-jar","chizuhara.jar"]